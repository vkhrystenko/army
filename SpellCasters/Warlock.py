from SpellCasters.SpellCaster import SpellCaster
from Units.Demon import Demon
from Units.Point import Point
from Spells import FireBall

class Warlock(SpellCaster):
	def __init__(self, name, hp, dmg, armor=0, location=Point(0,0), mana=20):
		super(Warlock, self).__init__(name, hp, dmg, armor, location, mana)
		self._spell = FireBall.FireBall()
		self._slave = None

	def sumon(self):
		super(SpellCaster, self).ensureisalive()

		self._slave = Demon("Demon", 100, 20, self)

	def demon(self):
		super(SpellCaster, self).ensureisalive()

		return self._slave

	def __str__(self):
		return super(Warlock, self).__str__()
	
	def __del__(self):
		del self._slave