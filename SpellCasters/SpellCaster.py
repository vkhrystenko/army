from Units.Excep import OutOfManaException
from Units.Unit import Unit
from Units.Point import Point
from Spells.Spell import Spell

import sys

class SpellCaster(Unit):


    def __init__(self, name, hitpoints, damage, armor=0, location=Point(0, 0), mana=20):
        super(SpellCaster, self).__init__(name, hitpoints, damage, armor, location)
        self._mana = mana
        self._manalimit = mana
        self._spell = None

    @property
    def mana(self):
        return self._mana

    @property
    def manalimit(self):
        return self._manalimit

    def manaspend(self, cost):
        self.ensureisalive()

        if cost > self.mana:
            raise OutOfManaException()

        self._mana -= cost

    def addmana(self, extra):
        self.ensureisalive()
        total = self.mana + extra

        if total > self._manalimit:
            self._mana = self._manalimit
            return
        self._mana = total

    def amplify(self):
        self._spell.amplify()

    def changespell(self, newspell):
        del self._spell
        self._spell = newspell

    def cast(self, target):
        self.manaspend(self._spell.cost)
        self._spell.action(target)
        target.counterattack(self)

    def attack(enemy):
        Unit.attack(enemy)


    def __del__(self):
        del self._spell

    def __str__(self):
        return super(SpellCaster, self).__str__() + " mana:(" + str(self.mana) + "/" + str(self.manalimit) + ")"
    	

