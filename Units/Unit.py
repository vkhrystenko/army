from Point import Point
import Excep

import sys

class Unit(object):
    # __slots__ = ['_x', '_y']

    def __init__(self, name, hitpoints, damage, armor=0, location=Point(0, 0)):
    	self._name = name
    	self._hitpoints = hitpoints
    	self._damage = damage
    	self._hitpointslimit = hitpoints
    	self._armor = armor
    	self._location = location
      
    def ensureisalive(self):
    	if self._hitpoints == 0:
        	raise UnitIsDeadException()
    
    @property
    def hitpoints(self):
    	return self._hitpoints

    @property
    def hitpointslimit(self):
    	return self._hitpointslimit

    @property
    def damage(self):
    	return self._damage

    @property
    def name(self):
   		return self._name

    @property
    def armor(self):
    	return self._armor

    @property
    def location(self):
    	return self._location

    def move(self, *args):
    	try:
    		assert(len(args) <= 2)
    	except AssertionError:
    		print "Too much input values!!!"
    		sys.exit()
    	if isinstance(args[0], Point):
    		self._location = args[0]
    	else:
    		self.move(Point(args[0],args[1]))

    def addhitpoints(self, hp):
    	self.ensureisalive()
    	total = self.hitpoints + hp

    	if  total > self.hitpointslimit:
        	self._hitpoints = self._hitpointslimit
        	return
    	self._hitPoints = total

	def upgradeunit(self, upgrade):
		self._hitpoints += upgrade
		self._hitpointslimit += upgrade
		self._armor += upgrade

    def takedamage(self, dmg):
    	self.ensureisalive()

        if self._armor != 0:
            newhits = self._armor + self.hitpoints
            if newhits > dmg:
                newhits  -= dmg
            else:
                self._hitpoints = 0
                self._armor = 0
                return

            if newhits > self._hitpoints:
                self._armor -= dmg
            else:
                self._armor = 0;
                self._hitpoints = self._newhits
 
            if self._armor < 0:
                self._armor = 0
    	else:
            if dmg > self._hitpoints:
                self._hitpoints = 0
                return
        self._hitpoints -= dmg

    def takemagicdamage(self, dmg):
        self.takedamage(dmg)


    def counterattack(self, enemy):
        self.ensureisalive()
        enemy.takedamage(self._damage/2)


    def attack(self, enemy):
    	self.ensureisalive()
    	if self.location == enemy.location:
        	enemy.takedamage(self._damage)
        	enemy.counterattack(self)
    	else: 
        	move(enemy.location)
        	enemy.takedamage(self._damage*1.3)
        	enemy.counterattack(self)

    def __str__(self):
    	return self.name + " with strength " + str(self.hitpoints) + " and damage " \
    			+  str(self.damage) + " and armor" + str(self.armor) + "\n" +'Located at ' \
    			+ self.location.__str__()

