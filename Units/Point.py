class Point(object):
    # __slots__ = ['_coord']
    
    def __init__(self, x=0, y=0):
        self._coord = [x, y]


    def __cmp__(self, other):
        # print "Here we have", cmp(self.coord, other.coord)
        return cmp(self.coord, other.coord)

    # def __eq__(self, other):
    #     return self.x == other.x and self.y == other.y

    # def __ne__(self, other):
    #     return not self == other

    def __str__(self):
        return "{}".format(self.coord)

    @property
    def coord(self):
        return self._coord

    # @coord.setter
    def setCoord(self, c, value):
        try:
            assert(c >= 0 and c <= 1)
            self.coord[c] = value
        except AssertionError:
            print "Outty)))"

    # @property
    # def x(self):
    #     return self._x

    # @x.setter
    # def setX(self, value):
    #     self._x = value

    # @property    
    # def y(self):
    #     return self._y

    # @y.setter    
    # def setY(self, value):
    #     self._y = value

    def distance(self, other):
        from math import hypot
        return hypot(self.coord[0] - other.coord[0], self.coord[1] - other.coord[1])
    
    def __del__(self):
        print "{} is deleted".format(self.__class__.__name__)