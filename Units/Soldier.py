from Point import Point
from Unit import Unit
import Excep

class Soldier(Unit):

	def __init__(self, name, hitpoints, damage, arm=0, location=Point(0,0)):
		super(Soldier, self).__init__(name, hitpoints, damage, arm, location)

	def attack(self, enemy):
		super(Soldier, self).attack(enemy)

	def __del__(self):
		pass

# if __name__ == "__main__":
# 	soldier = Soldier('Me', 1,3,4)
# 	soldier2 = Soldier('Me', 2,4,5)
# 	print soldier
# 	print soldier2
# 	soldier.attack(soldier2)
	