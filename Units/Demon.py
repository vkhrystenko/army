# from SpellCasters.Warlock import Warlock
from Units.Unit import Unit

class Demon(Unit):
	def __init__(self, name, hp, dmg, master):
		super(Demon, self).__init__(name, hp, dmg)
		self._master = master

	def attack(self, enemy):
		super(Demon,self).attack(enemy) 