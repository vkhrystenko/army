from Point import Point
from Unit import Unit
import Excep

class State(object):

	def __init__(self, name, hitpoints, damage):
		self._name = name
		self._hitpoints = hitpoints
		self._hitpointslimit = hitpoints
		self._damage = damage

class Verwolf(Unit):

	def __init__(self, name, hitpoints, damage):
		super(Verwolf, self).__init__(name, hitpoints, damage)
		self._human = State(name, hitpoints, damage)
		self._wolf = State("Wolf", hitpoints*1.5, damage*10)
		self._current = self._human

	def attack(self, enemy):
		super(Unit, self).attack(enemy)

	def changestate(self):	
		self.ensureisalive()

		if self._current == self._human:
			self._current = self._wolf
		else:
			self._current = self._human
    		self._name = self._current._name
    		self._hitpoints = self._current._hitpoints
    		self._hitpointslimit = self._current._hitpointslimit
    		self._damage = self._current._damage

	def __del__(self):
		del self._human
		del self._wolf
