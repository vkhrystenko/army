from Units.Unit import Unit
from Units.Point import Point
from Units.Soldier import Soldier
from Units.Verwolf import Verwolf
from SpellCasters.Warlock import Warlock


if __name__ == '__main__':
	soldier1 = Soldier("Soldier1", 180,20)
	soldier2 = Soldier("Soldier2", 150, 50, 20, Point(3, 4))
	verwolf = Verwolf("Man", 100, 10)
	warlock = Warlock("warlock", 120, 20, 100, Point(0,0), 400)

	print soldier1
	# print soldier2
	# print verwolf
	# print warlock
	print "========"
	# print warlock.__dict__
	warlock.sumon()
	warlock.demon().attack(soldier1)
	# print warlock.demon()
	# warlock.amplify()
	# warlock.cast(soldier1)
	# print "========"
	# print warlock
	print soldier1

	# verwolf.changestate()
	# print verwolf
	# warlock.sumon()

	soldier1.move(1,1)


