class Spell(object):
    cost = 30
    actioncost = 30

    def amplify(self):
        Spell.cost *= 1.5
        Spell.actioncost *= 2

    # @property
    # def cost(self):
    #     return Spell.cost

if __name__ == "__main__":
    spell = Spell()
    print Spell.cost